from injector import Module

from file_enrichment.context.main.domain.enriched_file.enriched_file_repository_t import (
    EnrichedFileRepositoryT,
)
from file_enrichment.context.main.domain.enrichment_repository_t import (
    EnrichmentRepositoryT,
)
from file_enrichment.context.main.infrastructure.sqlalchemy.file_enrichment.enriched_file_repository import (
    SqlAlchemyEnrichedFileRepository,
)
from file_enrichment.context.main.infrastructure.virustotal.enrichment_repository import (
    VirustotalEnrichmentRepository,
)


class FileEnrichmentModule(Module):
    def configure(self, binder):
        binder.bind(EnrichedFileRepositoryT, to=SqlAlchemyEnrichedFileRepository)
        binder.bind(EnrichmentRepositoryT, to=VirustotalEnrichmentRepository)
