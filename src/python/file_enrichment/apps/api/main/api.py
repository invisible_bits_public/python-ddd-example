from typing import Callable, List, Optional, Sequence, Type

from aiohttp import web
from aiohttp.web_app import Application
from injector import Injector, Module

from common.sql_alchemy.main.mapper import SqlAlchemyMapper
from common.utils.main.class_property import class_property
from file_enrichment.apps.api.main.routing import config_routing


class AppNotConfiguredError(Exception):
    ...


class Api:
    _app: Optional[Application] = None
    injector: Optional[Injector] = None
    middleware: Optional[Sequence[Callable]] = None

    @classmethod
    def config(
        cls, modules: List[Module], middleware: Optional[List] = None
    ) -> "Type[Api]":
        cls.injector = Injector(modules=modules)

        cls.middleware = middleware or []

        cls._app = web.Application(middlewares=cls.middleware)

        config_routing(cls._app, injector=cls.injector)

        mapping = cls.injector.get(SqlAlchemyMapper)
        mapping.load()

        return cls

    @classmethod
    def close(cls):
        ...

    @class_property
    def app(cls) -> Application:
        assert cls._app is not None
        try:
            return cls._app
        except AttributeError:
            raise AppNotConfiguredError("Test web app not configured. Exiting.")

    @classmethod
    def run(cls):
        # api_settings = cls.injector.get(ApiSettings)
        # web_bind = api_settings.web_bind
        # web_port = api_settings.web_port
        # web.run_app(cls._app, port=web_port, host=web_bind)
        web.run_app(cls._app, port=8081, host="0.0.0.0")
