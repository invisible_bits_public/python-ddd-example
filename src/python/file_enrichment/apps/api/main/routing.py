from aiohttp import web
from aiohttp.web_app import Application
from injector import Injector

from common.utils.main.regex import MD5_REGEX
from file_enrichment.apps.api.main.resource.resource import Resource
from file_enrichment.apps.api.main.resource.signature_detail_resource import (
    FileEnrichmentResourceDetail,
)


def config_routing(app: Application, injector: Injector):
    Resource.config(injector)
    routes = [
        web.view(f"/signature/{{signature:{MD5_REGEX}}}", FileEnrichmentResourceDetail)
    ]
    app.add_routes(routes)
