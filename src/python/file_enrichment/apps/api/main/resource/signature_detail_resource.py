from aiohttp import web
from sqlalchemy.orm import Session

from file_enrichment.apps.api.main.resource.resource import Resource
from file_enrichment.context.main.application.signature_detail_retriever_service import (
    EnrichedFileDetailService,
)
from file_enrichment.context.main.domain.enriched_file.signature import Signature


class FileEnrichmentResourceDetail(Resource):
    async def get(self):
        session = self._injector.get(Session)
        application_service = self._injector.get(EnrichedFileDetailService)

        signature_str = self.request.match_info["signature"]
        signature = Signature(value=signature_str)
        enriched_file = await application_service(signature)

        if not enriched_file:
            await self.close(session)
            return web.Response(text="Resource does not exist.", status=404)

        response = {
            "signature": enriched_file.signature.value,
            "status": enriched_file.status.value,
            "report": enriched_file.report,
        }

        await self.close(session)
        return web.json_response(response, status=200)
