from _asyncio import get_event_loop

import pytest
from sqlalchemy import MetaData
from sqlalchemy.engine import Engine
from sqlalchemy.orm import Session

from common.sql_alchemy.main.alembic import Alembic
from file_enrichment.apps.api.main.api import Api
from file_enrichment.apps.api.tests.modules import modules
from file_enrichment.context.main.infrastructure.sqlalchemy.file_enrichment.enriched_file_repository import (
    SqlAlchemyEnrichedFileRepository,
)
from file_enrichment.migration_tool import migrate


@pytest.fixture(scope="session")
def event_loop():
    loop = get_event_loop()
    loop.set_debug(True)
    yield loop
    loop.close()


@pytest.fixture()
def loop(event_loop):
    yield event_loop


@pytest.fixture(scope="session")
def api():
    app = Api.config(modules=modules)
    yield app
    app.close()


@pytest.fixture(scope="session")
def setup_db(api) -> Engine:
    engine = api.injector.get(Engine)
    alembic = Alembic(engine)
    migrate(alembic)
    yield engine
    alembic.drop_db()


@pytest.fixture()
async def client(aiohttp_client, setup_db, api):
    client = await aiohttp_client(api.app)
    yield client
    metadata: MetaData = api.injector.get(MetaData)
    engine: Engine = api.injector.get(Engine)
    for tbl in reversed(metadata.sorted_tables):
        engine.execute(tbl.delete())


@pytest.fixture(scope="session")
def sql_alchemy_enriched_file_repository(api, setup_db):
    session = api.injector.get(Session)
    repository = SqlAlchemyEnrichedFileRepository(session)
    yield repository
    session.rollback()
