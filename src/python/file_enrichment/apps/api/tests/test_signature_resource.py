import pytest
from hamcrest import assert_that
from hamcrest.core.core import is_
from sqlalchemy.orm import Session

from file_enrichment.context.tests.mothers.enriched_file_mother import (
    EnrichedFileMother,
)
from file_enrichment.context.tests.mothers.signature_mother import SignatureMother


@pytest.fixture
def random_signature():
    return SignatureMother()


@pytest.fixture
def random_enriched_file():
    yield EnrichedFileMother()


@pytest.fixture
async def one_file_enrichment_init(
    sql_alchemy_enriched_file_repository, api, random_enriched_file
):
    session = api.injector.get(Session)
    await sql_alchemy_enriched_file_repository.add(random_enriched_file)
    session.commit()
    yield
    await sql_alchemy_enriched_file_repository.remove(random_enriched_file)
    session.commit()


@pytest.mark.asyncio
async def test_enriched_file_detail_get_non_existing(client, random_signature):
    response = await client.get(f"/signature/{random_signature}")
    assert_that(response.status, is_(404))


@pytest.mark.asyncio
async def test_enriched_file_detail_get_existing(
    client, one_file_enrichment_init, random_enriched_file
):
    response = await client.get(f"/signature/{random_enriched_file.signature.value}")
    assert_that(response.status, is_(200))
