from file_enrichment.apps.api.main.api import Api
from file_enrichment.apps.api.main.modules import modules


def main():
    app = Api.config(modules=modules)
    app.run()


if __name__ == "__main__":
    main()
