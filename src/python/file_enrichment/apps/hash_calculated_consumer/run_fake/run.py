import asyncio

from injector import Injector

from file_enrichment.apps.hash_calculated_consumer.main.hash_calculated_consumer import (
    HashCalculatedConsumer,
)
from file_enrichment.apps.hash_calculated_consumer.run_fake.modules import modules

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    injector = Injector(modules=modules)
    worker = HashCalculatedConsumer(injector, loop)
    worker.config()
    loop.run_until_complete(worker.consume())
