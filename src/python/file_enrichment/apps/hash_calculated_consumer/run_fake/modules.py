from common.shared_kernel.main.infrastructure.aio_kafka.dependency_injection import (
    AiokafkaModule,
)
from common.sql_alchemy.main.dependency_injection import SqlAlchemyModule
from file_enrichment.apps.dependency_injection.dependency_injection_fake import (
    FileEnrichmentModule,
)
from file_enrichment.context.main.infrastructure.sqlalchemy.file_enrichment.mapping.enriched_signature_mapping import (
    EnrichedFileMapping,
)

modules = [
    AiokafkaModule(settings="file_enrichment/worker/aiokafka_settings"),
    SqlAlchemyModule(
        settings="file_enrichment/worker/db_settings", mappings=[EnrichedFileMapping]
    ),
    FileEnrichmentModule(),
]
