from dataclasses import dataclass

from injector import inject

from common.shared_kernel.main.domain.md5.md5 import MD5
from file_enrichment.context.main.domain.enriched_file.report import Report
from file_enrichment.context.main.domain.enrichment_repository_t import (
    EnrichmentRepositoryT,
)


@inject
@dataclass
class FakeEnrichmentRepository(EnrichmentRepositoryT):
    async def get_report(self, md5: MD5) -> Report:
        return Report("Fake report")
