from typing import Awaitable, Optional, no_type_check

from common.sql_alchemy.main.sqlalchemy_repository import SqlAlchemyRepositoryA
from common.utils.main.asyncio import force_async
from file_enrichment.context.main.domain.enriched_file.enriched_file import EnrichedFile
from file_enrichment.context.main.domain.enriched_file.enriched_file_repository_t import (
    EnrichedFileRepositoryT,
)
from file_enrichment.context.main.domain.enriched_file.signature import Signature


class AggregateNotFound(Exception):
    ...


class SqlAlchemyEnrichedFileRepository(SqlAlchemyRepositoryA, EnrichedFileRepositoryT):
    aggregate = EnrichedFile

    async def add(self, enriched_file: EnrichedFile):
        await self.persist(enriched_file)

    async def remove(self, enriched_file: EnrichedFile):
        await self.delete(enriched_file)

    async def get_all(self):
        result = await self.all()
        return result

    @no_type_check
    @force_async
    def get_by_id(self, signature: Signature) -> Awaitable[Optional[EnrichedFile]]:
        signature = self._query.get(signature)
        return signature
