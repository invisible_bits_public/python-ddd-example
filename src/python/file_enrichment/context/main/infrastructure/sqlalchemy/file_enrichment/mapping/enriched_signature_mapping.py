from sqlalchemy import Column, Table, Enum

from common.sql_alchemy.main.mapping import Mapping
from common.sql_alchemy.main.type import MD5Type, ValueObjectStrType
from file_enrichment.context.main.domain.enriched_file.enriched_file import EnrichedFile
from file_enrichment.context.main.domain.enriched_file.status import Status


class EnrichedFileMapping(Mapping):
    @property
    def table(self):
        return Table(
            "enriched_file",
            self._metadata,
            Column("signature", MD5Type(), primary_key=True),
            Column("status", Enum(Status), nullable=False),
            Column("report", ValueObjectStrType(), nullable=False),
        )

    @property
    def entity(self):
        return EnrichedFile
