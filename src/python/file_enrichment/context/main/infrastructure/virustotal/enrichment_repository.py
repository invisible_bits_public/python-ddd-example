from dataclasses import dataclass

from injector import inject

from common.shared_kernel.main.domain.md5.md5 import MD5
from common.virustotal.main.virustotal_client import VirustotalClient
from file_enrichment.context.main.domain.enriched_file.report import Report
from file_enrichment.context.main.domain.enrichment_repository_t import (
    EnrichmentRepositoryT,
)


@inject
@dataclass
class VirustotalEnrichmentRepository(EnrichmentRepositoryT):
    virustotal_client: VirustotalClient

    async def get_report(self, md5: MD5) -> Report:
        return await self.virustotal_client.get_report(md5)
