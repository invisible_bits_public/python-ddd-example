from dataclasses import dataclass
from typing import Optional

from injector import inject

from common.shared_kernel.main.domain.event.event_publisher_t import EventPublisherT
from file_enrichment.context.main.contract.enrichment_finished_domain_event import (
    EnrichmentFinishedDomainEvent,
)
from file_enrichment.context.main.domain.enriched_file.enriched_file import EnrichedFile
from file_enrichment.context.main.domain.enriched_file.enriched_file_repository_t import (
    EnrichedFileRepositoryT,
)
from file_enrichment.context.main.domain.enriched_file.report import Report
from file_enrichment.context.main.domain.enriched_file.signature import Signature
from file_enrichment.context.main.domain.enriched_file.status import Status
from file_enrichment.context.main.domain.enrichment_repository_t import (
    EnrichmentRepositoryT,
)


@inject
@dataclass
class FileSignatureAnalysisService:
    enrichment_repository: EnrichmentRepositoryT
    enriched_file_repository: EnrichedFileRepositoryT
    event_publisher: EventPublisherT

    async def __call__(self, enrich_id: Signature):
        enriched_file: Optional[EnrichedFile] = await self.enriched_file_repository.get_by_id(enrich_id)
        if enriched_file:
            return

        report: Report = await self.enrichment_repository.get_report(enrich_id)
        enriched_file = EnrichedFile.create(
            signature=enrich_id, status=Status.DONE, report=report
        )
        await self.enriched_file_repository.add(enriched_file)

        domain_events = enriched_file.pull_events()
        await self.event_publisher.publish(domain_events)
