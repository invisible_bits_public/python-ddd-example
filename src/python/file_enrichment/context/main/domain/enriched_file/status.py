from enum import Enum


class Status(Enum):
    DONE = 0
    PENDING = 1
    ERROR = 2
