from abc import ABCMeta, abstractmethod
from dataclasses import dataclass
from typing import Iterator, Optional

from file_enrichment.context.main.domain.enriched_file.enriched_file import EnrichedFile
from file_enrichment.context.main.domain.enriched_file.signature import Signature


@dataclass
class EnrichedFileRepositoryT(metaclass=ABCMeta):
    @abstractmethod
    async def add(self, enriched_file: EnrichedFile):
        ...

    @abstractmethod
    async def remove(self, enriched_file: EnrichedFile):
        ...

    @abstractmethod
    async def get_all(self) -> Iterator[EnrichedFile]:
        ...

    @abstractmethod
    async def get_by_id(self, id_: Signature) -> Optional[EnrichedFile]:
        ...
