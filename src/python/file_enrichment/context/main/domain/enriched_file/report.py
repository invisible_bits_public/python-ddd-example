from dataclasses import dataclass


@dataclass(frozen=True)
class Report:
    value: str
