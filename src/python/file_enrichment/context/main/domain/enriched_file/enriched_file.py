from dataclasses import dataclass

from common.shared_kernel.main.domain.aggregate.aggregate import AggregateRootA
from file_enrichment.context.main.contract.enrichment_finished_domain_event import (
    EnrichmentFinishedDomainEvent,
)
from file_enrichment.context.main.domain.enriched_file.report import Report
from file_enrichment.context.main.domain.enriched_file.signature import Signature
from file_enrichment.context.main.domain.enriched_file.status import Status


@dataclass(eq=False)
class EnrichedFile(AggregateRootA):
    signature: Signature
    status: Status
    report: Report

    @classmethod
    def create(cls, signature, status, report):
        enriched_file = EnrichedFile(signature=signature, status=status, report=report)
        domain_event = EnrichmentFinishedDomainEvent(
            aggregate_id=signature.value, status=status.value, report=report.value
        )
        enriched_file.record(domain_event)
        return enriched_file
