from abc import ABCMeta, abstractmethod

from common.shared_kernel.main.domain.md5.md5 import MD5
from file_enrichment.context.main.domain.enriched_file.report import Report


class EnrichmentRepositoryT(metaclass=ABCMeta):
    @abstractmethod
    async def get_report(self, md5: MD5) -> Report:
        ...
