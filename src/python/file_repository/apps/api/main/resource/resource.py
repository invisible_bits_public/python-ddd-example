from typing import Optional

from aiohttp.abc import Request
from aiohttp.web import View
from injector import Injector

from common.utils.main.asyncio import force_async


class Resource(View):
    _injector: Optional[Injector] = None

    def __init__(self, request: Request):
        assert self._injector is not None
        super().__init__(request)

    @force_async
    def _session_close(self, session):
        session.commit()
        session.close()

    async def close(self, session):
        await self._session_close(session)

    @classmethod
    def config(cls, injector: Injector):
        cls._injector = injector
