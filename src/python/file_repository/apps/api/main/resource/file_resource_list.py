from typing import Iterator, Mapping

from aiohttp import web
from sqlalchemy.orm import Session

from file_repository.apps.api.main.resource.resource import Resource
from file_repository.context.main.application.file_info_list_retriever_service import FileInfoListRetrieverService
from file_repository.context.main.domain.file.file_info import FileInfo


class FileResourceList(Resource):

    @staticmethod
    def _serialize_file_info(file_info: FileInfo) -> Mapping:
        response = {
            "id": file_info.id_.value,
            "status": file_info.name.value,
            "report": file_info.path.value,
            "md5": file_info.md5.value if file_info.md5 else "null",
        }
        return response

    def _serialize_all(self, file_info_list: Iterator[FileInfo]) -> Iterator[Mapping]:
        serialized_file_infos = list(map(self._serialize_file_info, file_info_list))
        return serialized_file_infos

    async def get(self):
        application_service = self._injector.get(FileInfoListRetrieverService)
        session = self._injector.get(Session)

        file_info_list: Iterator[FileInfo] = await application_service()
        serialized = self._serialize_all(file_info_list)

        await self.close(session)
        return web.json_response(serialized, status=200)
