from aiohttp import web
from aiohttp.web_app import Application
from injector import Injector

from common.utils.main.regex import UUID_REGEX
from file_repository.apps.api.main.resource.file_resource_detail import FileResourceDetail
from file_repository.apps.api.main.resource.file_resource_list import FileResourceList
from file_repository.apps.api.main.resource.resource import Resource


def config_routing(app: Application, injector: Injector):
    Resource.config(injector)
    routes = [
        web.view("/file", FileResourceList),
        web.view(f"/file/{{id:{UUID_REGEX}}}", FileResourceDetail),
    ]
    app.add_routes(routes)
