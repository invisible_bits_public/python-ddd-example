import pytest
from hamcrest.core import assert_that
from hamcrest.core.core import is_
from sqlalchemy.orm import Session

from file_repository.context.tests.mothers.file_id_mother import FileIDMother
from file_repository.context.tests.mothers.file_info_mother import FileInfoMother


@pytest.fixture
def random_file_id():
    return FileIDMother()


@pytest.fixture
def random_file_info():
    yield FileInfoMother()


@pytest.fixture
async def one_file_info_init(
        sql_alchemy_file_info_repository, api, random_file_info
):
    session = api.injector.get(Session)
    await sql_alchemy_file_info_repository.add(random_file_info)
    session.commit()
    yield
    await sql_alchemy_file_info_repository.remove(random_file_info)
    session.commit()


@pytest.mark.asyncio
async def test_file_info_detail_get_non_existing(client, one_file_info_init, random_file_id):
    response = await client.get(f"/file/{random_file_id}")
    assert_that(response.status, is_(404))


@pytest.mark.asyncio
async def test_file_info_detail_get_existing(
        client, one_file_info_init, random_file_info
):
    response = await client.get("/file")
    assert_that(response.status, is_(200))
