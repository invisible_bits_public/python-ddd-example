from pathlib import Path

from common.sql_alchemy.main.alembic import Alembic
from common.utils.main.pathlib import get_dir


def migrate(alembic: Alembic):
    migrations_dir = get_dir(Path(__file__)) / Path("migrations")
    alembic.migrate(migrations_dir)
