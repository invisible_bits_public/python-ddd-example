import factory

from file_repository.context.main.domain.file.file_name import FileName


class FileNameMother(factory.Factory):
    value = factory.Faker("file_name")

    class Meta:
        model = FileName
