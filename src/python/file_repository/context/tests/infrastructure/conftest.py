from _asyncio import get_event_loop

import pytest
from injector import Injector
from sqlalchemy.engine import Engine

from common.sql_alchemy.main.alembic import Alembic
from common.sql_alchemy.main.mapper import SqlAlchemyMapper
from file_repository.apps.api.main.api import Api
from file_repository.apps.api.tests.modules import modules
from file_repository.migration_tool import migrate


@pytest.fixture(scope="session")
def event_loop():
    loop = get_event_loop()
    loop.set_debug(True)
    yield loop
    loop.close()


@pytest.fixture()
def loop(event_loop):
    yield event_loop


@pytest.fixture(scope="session")
def api():
    app = Api.config(modules=modules)
    yield app
    app.close()


@pytest.fixture(scope="session")
def injector():
    injector = Injector(modules=modules)
    yield injector


@pytest.fixture(scope="session")
def setup_db(injector):
    engine = injector.get(Engine)
    mapper = injector.get(SqlAlchemyMapper)
    mapper.load()
    alembic = Alembic(engine)
    migrate(alembic)
    yield engine
    alembic.drop_db()
