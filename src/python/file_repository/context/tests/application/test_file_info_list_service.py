import pytest

from common.async_test_utils.main.mocks import AsyncMock
from file_repository.context.main.application.file_info_list_retriever_service import FileInfoListRetrieverService


@pytest.mark.asyncio
async def test_file_info_list_service():
    file_info_repository = AsyncMock()
    file_info_lister_service = FileInfoListRetrieverService(
        file_info_repository=file_info_repository
    )
    await file_info_lister_service()
    file_info_repository.get_all.assert_called_once()
