from injector import Module

from file_repository.context.main.domain.file.file_info_repository_t import (
    FileInfoRepositoryT,
)
from file_repository.context.main.domain.file.file_store_client_t import (
    FileStoreClientT,
)
from file_repository.context.main.infrastructure.minio.minio_file_store_client import (
    MinioFileStoreClient,
)
from file_repository.context.main.infrastructure.sql_alchemy.file_info.file_info_repository import (
    SqlAlchemyFileInfoRepository,
)


class FileInfoModule(Module):
    def configure(self, binder):
        binder.bind(FileInfoRepositoryT, to=SqlAlchemyFileInfoRepository)
        binder.bind(FileStoreClientT, to=MinioFileStoreClient)
