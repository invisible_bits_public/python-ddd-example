from dataclasses import dataclass

from injector import Module, provider, singleton

from file_repository.context.main.infrastructure.minio.settings import MinioSettings


@dataclass
class MinioModule(Module):
    settings: str

    @singleton
    @provider
    def minio_settings(self) -> MinioSettings:
        return MinioSettings(name=self.settings)
