from hashlib import md5
from io import BytesIO

import boto3
from boto3_type_annotations.s3 import Client, ServiceResource, Bucket
from botocore.config import Config
from injector import inject

from common.shared_kernel.main.domain.md5.md5 import MD5
from common.utils.main.asyncio import force_async
from file_repository.context.main.domain.file.file_path import FilePath
from file_repository.context.main.domain.file.file_store_client_t import (
    FileStoreClientT,
)
from file_repository.context.main.infrastructure.minio.settings import MinioSettings


class MinioFileStoreClient(FileStoreClientT):
    @inject
    def __init__(self, settings: MinioSettings):
        self.client: Client = boto3.client("s3")
        self.resource: ServiceResource = boto3.resource(
            "s3",
            endpoint_url=settings.url,
            aws_access_key_id=settings.aws_access_key_id,
            aws_secret_access_key=settings.aws_secret_access_key,
            config=Config(signature_version="s3v4"),
            region_name="us-east-1",
        )

    @force_async
    def calculate_md5(self, path: FilePath) -> MD5:
        bucket = path.get_root()
        file = path.get_path_from_root()
        in_mem_file = BytesIO()
        object_reference = self.resource.Object(bucket, file)
        object_reference.download_fileobj(in_mem_file)
        file_md5: str = md5(in_mem_file.getvalue()).hexdigest()
        in_mem_file.close()
        return MD5(file_md5)
