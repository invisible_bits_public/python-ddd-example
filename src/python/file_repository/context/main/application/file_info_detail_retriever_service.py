from dataclasses import dataclass
from typing import Optional

from injector import inject

from file_repository.context.main.domain.file.file_id import FileID
from file_repository.context.main.domain.file.file_info import FileInfo
from file_repository.context.main.infrastructure.sql_alchemy.file_info.file_info_repository import (
    SqlAlchemyFileInfoRepository,
)


@inject
@dataclass
class FileInfoDetailRetrieverService:
    file_info_repository: SqlAlchemyFileInfoRepository

    async def __call__(self, file_id: FileID) -> Optional[FileInfo]:
        file_info = await self.file_info_repository.get_by_id(id_=file_id)
        return file_info
