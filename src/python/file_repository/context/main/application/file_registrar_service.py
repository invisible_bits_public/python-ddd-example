from dataclasses import dataclass

from injector import inject

from common.shared_kernel.main.domain.event.event_publisher_t import EventPublisherT
from common.shared_kernel.main.domain.md5.md5 import MD5
from file_repository.context.main.contract.hash_calculated_domain_event import (
    HashCalculatedDomainEvent,
)
from file_repository.context.main.domain.file.file_id import FileID
from file_repository.context.main.domain.file.file_info import FileInfo
from file_repository.context.main.domain.file.file_info_repository_t import (
    FileInfoRepositoryT,
)
from file_repository.context.main.domain.file.file_name import FileName
from file_repository.context.main.domain.file.file_path import FilePath
from file_repository.context.main.domain.file.file_store_client_t import (
    FileStoreClientT,
)


@inject
@dataclass
class FileRegistrarService:
    file_info_repository: FileInfoRepositoryT
    file_store_client: FileStoreClientT
    event_publisher: EventPublisherT

    async def __call__(self, id_: FileID, name: FileName, path: FilePath):
        file_info = FileInfo.create(id_=id_, name=name, path=path)
        md5: MD5 = await self.file_store_client.calculate_md5(file_info.path)
        file_info.add_m5(md5)

        await self.file_info_repository.add(file_info)

        domain_events = file_info.pull_events()
        await self.event_publisher.publish(domain_events)
