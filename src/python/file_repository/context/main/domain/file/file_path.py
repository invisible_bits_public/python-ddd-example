from dataclasses import dataclass


@dataclass(frozen=True)
class FilePath:
    value: str

    def get_root(self):
        value = self.value.split("/")[0]
        return value

    def get_path_from_root(self):
        value = "/".join(self.value.split("/")[1:])
        return value
