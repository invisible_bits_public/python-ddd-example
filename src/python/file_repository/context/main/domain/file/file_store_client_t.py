from typing import Awaitable

from common.shared_kernel.main.domain.md5.md5 import MD5
from file_repository.context.main.domain.file.file_path import FilePath


class FileStoreClientT:
    async def calculate_md5(self, path: FilePath) -> MD5:
        ...
