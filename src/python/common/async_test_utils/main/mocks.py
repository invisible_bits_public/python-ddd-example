import inspect
import unittest.mock
from inspect import Parameter
from types import MappingProxyType
from typing import Callable, Mapping
from unittest.mock import MagicMock, Mock

unittest.mock._allowed_names.add("async_side_effect")
unittest.mock._allowed_names.add("async_return_value")


class AsyncMock(MagicMock):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._return_is_async = False

    @staticmethod
    async def _mock_awaitable(value):
        return value

    def __get_async_side_effect(self):
        return self._mock_delegate

    def __set_async_side_effect(self, value):
        super()._NonCallableMock__set_side_effect(value)
        self._return_is_async = True

    async_side_effect = property(__get_async_side_effect, __set_async_side_effect)

    def __get_async_return_value(self):
        return super()._NonCallableMock__get_return_value()

    def __set_async_return_value(self, value):
        super()._NonCallableMock__set_return_value(value)
        self._return_is_async = True

    async_return_value = property(__get_async_return_value, __set_async_return_value)

    def _mock_call(_mock_self, *args, **kwargs):
        self = _mock_self
        ret_val = super()._mock_call(*args, **kwargs)

        if self._return_is_async:
            return self._mock_awaitable(ret_val)
        else:
            return ret_val

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        pass

    async def __call__(self, *args, **kwargs):
        return super(AsyncMock, self).__call__(*args, **kwargs)


class AsyncContextManagerMock(MagicMock):
    async def __aenter__(self):
        return self.aenter

    async def __aexit__(self, *args):
        pass


def cleanup_args(params: Mapping[str, Parameter]) -> MappingProxyType:
    cleanup_dict = {}
    reserved_words = ["self", "kwargs", "args", "*", "**"]
    for k, v in params.items():
        if k in reserved_words:
            continue
        if v.default != Parameter.empty:
            continue
        cleanup_dict[k] = v
    return MappingProxyType(cleanup_dict)


def assert_called_with_signature(my_mock: Mock, function: Callable):
    signature = inspect.signature(function)
    called_args = set(my_mock.call_args[1].keys())
    mandatory_args = set(cleanup_args(signature.parameters).keys())
    assert mandatory_args.intersection(called_args) == mandatory_args
