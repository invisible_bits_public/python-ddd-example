import abc
import logging
from typing import Optional

from sqlalchemy import MetaData, Table
from sqlalchemy.exc import ArgumentError
from sqlalchemy.orm import Mapper, mapper

logger = logging.getLogger(__name__)


class Mapping:
    def __init__(self, metadata: MetaData):
        self._metadata = metadata

    @property
    @abc.abstractmethod
    def table(self) -> Table:
        ...

    @property
    @abc.abstractmethod
    def entity(self):
        ...

    def create(self) -> Optional[Mapper]:
        try:
            return mapper(self.entity, self.table)
        except ArgumentError:
            logger.error(f"{self.entity.__class__.__name__} already mapped")
            return None
