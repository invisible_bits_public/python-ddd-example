from typing import Sequence, Type

from injector import Module, provider, singleton
from sqlalchemy import MetaData, create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.orm import Session, sessionmaker
from sqlalchemy_utils import create_database, database_exists

from common.injector_ext.main.request_scope import request
from common.shared_kernel.main.domain.uuid import uuid
from common.sql_alchemy.main.db_settings import DbSettings
from common.sql_alchemy.main.mapper import SqlAlchemyMapper
from common.sql_alchemy.main.mapping import Mapping


class SqlAlchemyModule(Module):
    def __init__(
        self, settings: str, mappings: Sequence[Type[Mapping]], testing: bool = False
    ):
        self._mappings = mappings
        self._settings = settings
        self._testing = testing

    @singleton
    @provider
    def mapper(self, metadata: MetaData) -> SqlAlchemyMapper:
        mapper = SqlAlchemyMapper(mappings=self._mappings, metadata=metadata)
        return mapper

    @singleton
    @provider
    def db_settings(self) -> DbSettings:
        db_settings = DbSettings(self._settings)
        return db_settings

    @singleton
    @provider
    def engine(self, db_settings: DbSettings) -> Engine:
        url = db_settings.url
        if self._testing is True:
            random_path = str(uuid.uuid4()).replace("-", "")
            url += f"-{random_path}"
        if not database_exists(url):
            create_database(url)
        engine = create_engine(url)
        return engine

    @singleton
    @provider
    def metadata(self, engine: Engine) -> MetaData:
        metadata = MetaData(bind=engine)
        return metadata

    @request
    @provider
    def session(self, db: Engine) -> Session:
        session_factory = sessionmaker(bind=db, autoflush=False)
        session: Session = session_factory()
        return session
