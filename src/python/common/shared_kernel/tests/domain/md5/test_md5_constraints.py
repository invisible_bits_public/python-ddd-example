import pytest
from hamcrest import calling
from hamcrest.core import assert_that
from hamcrest.core.core import raises

from common.shared_kernel.main.domain.md5.md5 import (
    MD5,
    InvalidMD5Symbols,
    InvalidMD5Length,
)

correct = [("0123456789abcdef0123456789abcdef", MD5), ("a" * 32, MD5)]

violate_constraints = [
    ("0123456789ABcdef0123456789abcdef", InvalidMD5Symbols),
    ("0123456789ABcdef012345bhFGabcdef", InvalidMD5Symbols),
    ("a" * 64, InvalidMD5Length),
    ("a" * 16, InvalidMD5Length),
]


@pytest.mark.parametrize("test_input,expected", correct)
def test_md5_symbols_correct(test_input, expected):
    assert_that(isinstance(MD5(test_input), expected))


@pytest.mark.parametrize("test_input,expected", violate_constraints)
def test_md5_symbols_raises(test_input, expected):
    assert_that(calling(MD5).with_args(test_input), raises(expected))
