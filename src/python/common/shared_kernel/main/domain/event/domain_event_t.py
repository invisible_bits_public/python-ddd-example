from dataclasses import dataclass

from common.shared_kernel.main.domain.message import MessageT
from common.utils.main.no_default_attributes_dataclasses import NoDefaultVar, no_default


@dataclass(frozen=True)
class DomainEventT(MessageT):
    aggregate_id: NoDefaultVar[str] = no_default

    @property
    def sub_type(self) -> str:
        raise NotImplementedError

    @property
    def application(self) -> str:
        raise NotImplementedError
