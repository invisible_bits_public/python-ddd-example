from dataclasses import dataclass, field

from common.shared_kernel.main.domain.uuid.uuid import UUID


@dataclass(frozen=True)
class MessageT:
    message_id: str = field(default_factory=UUID.random_str)
    # TODO: This must by set from the event publisher settings

    @property
    def sub_type(self) -> str:
        raise NotImplementedError

    @property
    def application(self) -> str:
        raise NotImplementedError

    @property
    def message_type(self) -> str:
        return f"{self.application}.{self.sub_type}"
