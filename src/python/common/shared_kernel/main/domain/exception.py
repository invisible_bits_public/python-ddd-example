from abc import ABCMeta, abstractmethod


class DomainErrorT(Exception, metaclass=ABCMeta):
    @property
    @abstractmethod
    def message(self):
        ...


class ValidationError(DomainErrorT):
    message = "Generic Validation Error"

    def __init__(self, value):
        super(ValidationError, self).__init__(self.message % value)
