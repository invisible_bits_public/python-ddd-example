from dataclasses import dataclass
from uuid import UUID as PY_UUID
from uuid import uuid4

from common.shared_kernel.main.domain.exception import ValidationError


class UUIDValidationError(ValidationError):
    message = "%s is an invalid id"


@dataclass(frozen=True)
class UUID:
    value: str

    def __post_init__(self):
        try:
            PY_UUID(self.value)
        except ValueError:
            raise UUIDValidationError(self.value)

    @classmethod
    def random(cls):
        return cls(str(uuid4()))

    @classmethod
    def random_str(cls):
        return str(uuid4())
