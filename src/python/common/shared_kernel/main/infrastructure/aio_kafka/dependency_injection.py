from injector import Module, provider, singleton

from common.injector_ext.main.request_scope import request
from common.shared_kernel.main.domain.event.event_publisher_t import EventPublisherT
from common.shared_kernel.main.infrastructure.aio_kafka.aiokafka_settings import (
    AiokafkaSettings,
)
from common.shared_kernel.main.infrastructure.aio_kafka.event_publisher import (
    AiokafkaEventPublisher,
)


class AiokafkaModule(Module):
    def __init__(self, settings: str):
        self._settings = settings

    def configure(self, binder):
        binder.bind(EventPublisherT, to=AiokafkaEventPublisher, scope=request)

    @singleton
    @provider
    def settings(self) -> AiokafkaSettings:
        settings = AiokafkaSettings(self._settings)
        return settings
