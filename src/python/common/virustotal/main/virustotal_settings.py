from common.conf_settings.main.settings import BaseSettings


class VirustotalSettings(BaseSettings):
    @property
    def mapping(self):
        return {"api_key": str}
