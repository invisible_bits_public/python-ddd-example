from injector import Module, singleton, provider

from common.virustotal.main.virustotal_settings import VirustotalSettings


class VirustotalModule(Module):
    def __init__(self, settings: str):
        self._settings = settings

    @singleton
    @provider
    def virustotal_settings(self) -> VirustotalSettings:
        virustotal_settings = VirustotalSettings(self._settings)
        return virustotal_settings
