TEST_PATH=./
docker-pants := docker-compose -f docker-compose.pants.yml
docker-pants-run := $(docker-pants) run --rm pants
docker-pants-run-dist := $(docker-pants-run) ./dist/
docker-pants-run-cmd := $(docker-pants-run) ./pants

init:
	chmod +x ./scripts/unix/init_development_environment.sh
	./scripts/unix/init_development_environment.sh
	make run-all

run-all:
	$(docker-pants) pull
	make docker-pants-bundle
	make docker-pants-migrations
	make docker-run-all

clean: clean-pyc
	find . -name ".pytest_cache" | xargs rm -rf

clean-pyc:
	find . -name '*.pyc' -delete
	find . -name '*.pyo' -delete

clean-build:
	rm --force --recursive build/
	rm --force --recursive dist/
	rm --force --recursive *.egg-info

docker-pants-run-test:
	$(docker-pants-run-cmd) test ${args}\:\:

docker-pants-run-unit-test:
	$(docker-pants-run-cmd) --tag=unit-test test \:\:

docker-pants-run-integration-test:
	$(docker-pants-run-cmd) --tag=integration-test test \:\:

docker-pants-run-acceptance-test:
	$(docker-pants-run-cmd) --tag=acceptance-test test \:\:

docker-pants-create-binary:
	$(docker-pants-run-cmd) binary \:\:

docker-pants-bundle:
	$(docker-pants-run-cmd) bundle \:\:

docker-pants-run-pax:
	$(docker-pants-run-dist)${args}

docker-pants-format:
	$(docker-pants-run) black .

docker-pants-clean:
	$(docker-pants) down

docker-pants-prune:
	$(docker-pants) down -v

docker-pants-run-inside:
	$(docker-pants-run) ${args}

docker-pants-migrations:
	$(docker-pants-run) alembic -c src/python/file_repository/alembic.ini upgrade head
	$(docker-pants-run) alembic -c src/python/file_enrichment/alembic.ini upgrade head

docker-pants-logs:
	$(docker-pants) logs

docker-pants-start-db:
	$(docker-pants) up -d postgres

docker-run-all:
	docker-compose up -d topics-ui file-repository-minio-listener file-repository-api file-enrichment-api file-enrichment-worker-fake

docker-run-all-with-vt:
	docker-compose up -d topics-ui file-repository-minio-listener file-repository-api file-enrichment-api file-enrichment-worker

docker-pants-stop:
	$(docker-pants) stop

docker-pants-bash:
	$(docker-pants-run) /bin/bash

docker-pants-ps:
	$(docker-pants) ps

docker-pants-build:
	$(docker-pants) build
